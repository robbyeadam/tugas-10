1.buat Database


create Database myShop;
 

2. Membuat Table

use myShop;
create table Users(
    -> id int(9) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

 describe Users;

 +----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(9)       | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+

create table Categories(
    -> id int(9) primary key auto_increment,
    -> name varchar(255)
    -> );

describe Categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(9)       | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+

 create table Items(
    -> id int(9) primary key auto_increment,
    -> name varchar(255),
    -> price int(8),
    -> stock int(3),
    -> category_id int(9),
    -> foreign key(Category_id) references Users(id)
    -> );

 describe Items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(9)       | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| price       | int(8)       | YES  |     | NULL    |                |
| stock       | int(3)       | YES  |     | NULL    |                |
| category_id | int(9)       | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+

3. Memasukkan Data pada Table
 

 insert into Users(name,email,password) values
    -> ("John Doe","John@doe.com","john123");

 insert into Users(name,email,password) values
    -> ("Jon Cena","Jon@Cena.com","456Cena");  

select * from Users;
+----+----------+--------------+----------+
| id | name     | email        | password |
+----+----------+--------------+----------+
|  1 | John Doe | John@doe.com | john123  |
|  2 | Jon Cena | Jon@Cena.com | 456Cena  |
+----+----------+--------------+----------+    

insert into Categories(name) values
    -> ("gadget"),("cloth"),("men"),("women"),("branded");

select * from Categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+

insert into Items(name,description,price,stock,category_id) values
    -> ("Sumsang b50", "hape keren dari merek sumsang",4000000,100,1),
    ("Uniklooh", "baju keren dari brand ternama",500000,50,2),
    ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

select * from Items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+    

4. Mengambil Data dari Database

    a. Mengambil data users (Sajikan semua field pada table users KECUALI password nya.)
    
        select id,name,email from Users;
        +----+----------+--------------+
        | id | name     | email        |
        +----+----------+--------------+
        |  1 | John Doe | John@doe.com |
        |  2 | Jon Cena | Jon@Cena.com |
        +----+----------+--------------+

    b. Mengambil data items
        Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

        select * from Items where price > 1000000 ;
        +----+-------------+-----------------------------------+---------+-------+-------------+
        | id | name        | description                       | price   | stock | category_id |
        +----+-------------+-----------------------------------+---------+-------+-------------+
        |  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
        |  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
        +----+-------------+-----------------------------------+---------+-------+-------------+    

        Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

        select * from Items where name Like '%sang%';
        +----+-------------+-------------------------------+---------+-------+-------------+
        | id | name        | description                   | price   | stock | category_id |
        +----+-------------+-------------------------------+---------+-------+-------------+
        |  1 | Sumsang b50 | hape keren dari merek sumsang | 4000000 |   100 |           1 |
        +----+-------------+-------------------------------+---------+-------+-------------+

    c. Menampilkan data items join dengan kategori 

    select Items.name, Items.description, Items.price, Items.stock, Items.category_id , Categories.name
    -> from Items inner join Categories on Items.category_id = Categories.id;
    +-------------+-----------------------------------+---------+-------+-------------+--------+
    | name        | description                       | price   | stock | category_id | name   |
    +-------------+-----------------------------------+---------+-------+-------------+--------+
    | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
    | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
    | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
    +-------------+-----------------------------------+---------+-------+-------------+--------+

5. Mengubah Data dari Database

update Items set price= 2500000 where name = "Sumsang b50";

MariaDB [myShop]> select * from Items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+




